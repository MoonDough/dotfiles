{ pkgs ? import <nixpkgs> {} }:

let
  myEmacs = pkgs.emacs; 
  emacsWithPackages = (pkgs.emacsPackagesFor myEmacs).emacsWithPackages; 
in
  emacsWithPackages (epkgs: (with epkgs.melpaStablePackages; [ 
  #-- Stable
    magit                       # A Git porcelain inside Emacs.
    magit-todos                 # Show source file TODOs in Magit
    magit-org-todos             # Add local todo items to the magit status buffer
    #helm                        # Helm is an Emacs incremental and narrowing framework
    #helm-nixos-options          # Helm Interface for nixos-options
    nix-mode                    # Major mode for editing .nix files
    nixos-options               # Major mode for editing .nix files
    haskell-mode                # A Haskell editing mode
    nix-haskell-mode            # haskell-mode integrations for Nix
    org-superstar               # Prettify headings and plain lists in Org mode
    smartparens                 # Automatic insertion and wrapping  with user defined pairs.
    company                     # Modular text completion framework
    
  #-- Themes
    gruvbox-theme
    material-theme
    moe-theme                  
    zenburn-theme
    modus-themes                # Current default theme

    
  ]) ++ (with epkgs.melpaPackages; [ 
  #-- Standard
    #haskell-mode
    lsp-mode
    lsp-ui                      # UI modules for lsp-mode
    lsp-haskell
    lsp-latex
    lsp-ivy                     # LSP ivy integration
    hydra                       # Make bindings that stick around.
    avy                         # Jump to arbitrary positions in visible text and select text fast
    ccls                        # ccls client for lsp-mode
    ivy-xref                    # Ivy interface for xref results
    dap-mode                    # Debug Adapter Protocol mode
    yasnippet                   # Yet another snippet extension for emacs.
    #helm-nixos-options
    org-auto-tangle             # Automatically and Asynchronously tangles org files on save
    auto-org-md
    flycheck
    flycheck-haskell
    flymake-haskell-multi
    # flycheck-grammarly
    flycheck-hdevtools
    flyspell-correct            # Correcting words with flyspell via custom interface
    langtool                    # Grammar check utility using Language Tool
    projectile
    ivy
    ivy-rich
    ivy-prescient               # prescient.el + Ivy
    swiper
    counsel
    ivy-posframe
    smex
    use-package
    which-key
    # org-bullets
    toc-org
    neotree
    emojify
    dashboard
    all-the-icons
    all-the-icons-dired
    all-the-icons-ivy
    all-the-icons-ivy-rich
    all-the-icons-ibuffer
    htmlize                     # Convert buffer text and decorations to HTML.
    markdown-mode
    ghci-completion
    nov
    ox-epub
    ox-report
    mermaid-mode
    ob-mermaid
    general
    latex-pretty-symbols
    latex-math-preview
    highlight-numbers           # Highlight numbers in source code
    doom-modeline               # A minimal and modern mode-line
    ranger                      # Make dired more like ranger
    dired-single
    rainbow-delimiters          # Highlight brackets according to depth    
    counsel                     # Various completion functions using Ivy:
    helpful                     # A better *help* buffer
    # tree-sitter                 # Incremental parsing system
    # tree-sitter-langs           # Grammar bundle for tree-sitter
    # tsc                         # Core Tree-sitter APIs
    org-roam                    # A database abstraction layer for Org-mode
    org-roam-ui                 # User Interface for Org-roam
    org-roam-timestamps         # Keep track of modification times for org-roam
    plantuml-mode               # Major mode for PlantUML
    
  ]) ++ (with epkgs.elpaPackages; [ 
    auctex         # ; LaTeX mode
    beacon         # ; highlight my cursor when scrolling
    nameless       # ; hide current package name everywhere in elisp code
    setup	   # Helpful Configuration Macro
  ]) ++ [
    # pkgs.notmuch   # From main packages set 
  ])
