{
    "output": "eDP-1",
    "layer": "bottom",
    "position": "top",
    "modules-left": [
    	"battery",
	"idle_inhibitor",
        "sway/workspaces",
        "sway/mode"
    ],
    "modules-center": [
        "clock"
    ],
    "modules-right": [
        "cpu",
        "temperature",
        "memory",
        "disk",
        "network",
        "pulseaudio",
	"tray"
    ],
    "sway/workspaces": {
        "disable-scroll": false,
        "all-outputs": true,
        "format": "{name}"
    },
    "sway/mode": {
        "format": "<span style=\"italic\">{}</span>"
    },
    "clock": {
        "format": "{:%a %m/%d  %R}",
        "tooltip-format": "<big>{:%B}</big>\n<tt><small>{calendar}</small></tt>"
    },
    "cpu": {
        "format": "{usage}% ",	
	"states": {
           "good": 40,
	   "warning": 60,
	   "critical": 85
	}
    },
    "temperature": {
        "thermal-zone": 2,
        "hwmon-path": "/sys/class/hwmon/hwmon0/temp2_input",
        "critical-threshold": 110,
        "format-critical": "!{temperatureF}°! {icon}",
        "format": "{temperatureF}° {icon}",
        "format-icons": [ "" ]
    },
    "memory": {
        "format": "{percentage}% ",
	"tooltip-format": "{used:0.2f}/{total:0.2f} GiB"
    },
    "disk": {
        "states": {
            "good": 0,
            "warning": 70,
            "critical": 95
        },
        "interval": 30,
        "format": "{percentage_used}% ",
        "path": "/",
	"tooltip-format": "{used} / {total}"
    },
    "network": {
        "format-wifi": "{essid} ({signalStrength}%) ",
        "format-ethernet": "{ifname} ",
        "format-alt": "{ifname}: {ipaddr}/{cidr} ",
        "format-linked": "{ifname} (No IP) ",
        "format-disconnected": "Disconnected "
    },
    "pulseaudio": {
        "format": "{volume}% {icon} {format_source}",
        "format-bluetooth": "{volume}% {icon}  {format_source}",
        "format-muted": "婢 {format_source}",
        "format-source": "{volume}% ",
        "format-source-muted": "",
        "format-icons": {
            "headphones": "",
            "speaker": "蓼",
            "hdmi": "﴿",
            "headset": "",
            "phone": "",
            "portable": "",
            "car": "",
            "hifi": "",
            "default": [" ", " "]
        },
        "scroll-step": 1,
        "on-click": "pavucontrol"
    },
    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    },
    "tray": {
        "_icon-size": 28,
        "spacing": 2
    },
    "battery": {
        "bat": "BAT0",
        "interval": 60,
        "states": {
            "warning": 30,
            "critical": 15
        },
        "format": "{icon} {capacity}%",
	"format-charging": " {capacity}%",
        "format-icons": ["","", "", "", "", "", "", "", "", "", ""],
	"tooltip-format": " {capacity}%, {time}"
    }
}