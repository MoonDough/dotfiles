#+title:Sway Configuration 
#+author: Reymundo G\oacute{}mez
#+email: reymundo.r.gomez@outlook.com
#+date: <2022-06-04 sat>
#+description: Master Sway configuration file
#+startup: overview
#+property: header-args :tangle ./config
#+auto_tangle: t

May need to ~C-c~ on the above ~header-args~ for it to take effect.
* Autostart commands
+ mako: Notifcation daemon
+ udiskie: Automounter daemon
+ kanshi: Autorandr replacement
  * Kanshi needs to be restarted each time sway is reloaded otherwise it will change to default window positions 
+ dbus-...: NixOS sway wiki said to use this...
+ configure-gtk: NixOS sway wiki said to use this... 
#+begin_src conf
### autostart
exec mako
exec udiskie
exec_always "pkill kanshi; kanshi"
exec dbus-sway-environment
exec configure-gtk
exec blueman-applet
exec nm-tray     
#+end_src
* GTK-theme
#+begin_src conf
# set $gnome-schema org.gnome.desktop.interface

# exec_always {
#     gsettings set $gnome-schema gtk-theme 'Sweet'
#     gsettings set $gnome-schema icon-theme 'Your icon theme'
#     gsettings set $gnome-schema cursor-theme 'Your cursor Theme'
#     gsettings set $gnome-schema font-name 'Your font name'
# }
#+end_src
* Default variables
** sway
#+begin_src conf
#----=[ sway standard ]=----#
set $M       mod4
Set $M-A     $M+Alt
set $M-S     $M+Shift
set $M-S-A   $M-S+Alt
set $M-C     $M+Control
set $M-C-A   $M-C+Alt    
set $M-C-S   $M-C+Shift
set $M-C-S-A $M-C-S+Alt

set $lock "swaylock-fancy -t $USER"
# default_border pixel 3
# smart_borders on
smart_gaps on
gaps {
  inner 10
  outer 0
}
#+end_src
** applications
#+begin_src conf
#----=[ Default Apps ]=----#
set $term1    alacritty
set $term2    termonad
set $browser1 brave
set $browser2 firefox
set $fm       ranger
#+end_src
** Paths locations
#+begin_src conf
set $wallpapers_path $HOME/Pictures/Wallpaper
#+end_src
* General Key Bindings
** basics
| Keys              | description              | extra           |
|-------------------+--------------------------+-----------------|
| $mod+escape       | Kills current window     |                 |
| $mod+q            | recompiles & reload sway |                 |
| $mod+shift+escape | quits sway session       | warning message |
|                   |                          |                 |

#+begin_src conf
### Key bindings
#----=[ Basics ]=----#
#---- ESCAPE
bindsym $M+Escape kill
#bindsym $L2+Escape 
bindsym $M-C-S+Escape reload
bindsym $M-C-S-A+Escape exec swaynag -t warning \
		   		    -m 'Do you REALLY  want to exit sway? This will end your Wayland session.' \
				    -b 'Suspend' 'systemctl suspend' \
				    -b 'Lock' '$lock' \
				    -b 'Shutdown' 'systemctl -i poweroff' \
				    -b 'Reboot' 'systemctl -i reboot' \
				    -b 'Yes, exit sway' 'swaymsg exit' \
				    --button-background=#FF9100 \
                                --button-border-size=3px \
                                --border=#FCC99D \
                                --text=#092E47 \
                                --font=Hack Nerd Font Mono 16 \
                                --background=#282828
	
#---- PRINT
# Need to use ranger to select file path or filename
bindsym $M+Print       exec grim -g "$(slurp)" /home/MoonDough/Pictures/Screen_Shots/$(date +'%H:%M:%S.png')
bindsym $M-S+Print     exec grim /home/MoonDough/Pictures/Screen_Shots/$(date +'%H:%M:%S.png')
bindsym $M-C+Print     exec grim -g "$(slurp)" - | wl-copy
bindsym $M-C-S+Print   exec grim - | wl-copy
#+end_src
** Movement & Focus
#+begin_src conf
#----=[ Movement and Focus ]=----#
bindsym $M+a focus parent
bindsym $M-S+equal floating toggle
bindsym $M+equal focus mode_toggle

bindsym $M+Left focus left
bindsym $M+Down focus down
bindsym $M+Up focus up
bindsym $M+Right focus right

bindsym $M-S+Left  move left
bindsym $M-S+Down  move down
bindsym $M-S+Up    move up
bindsym $M-S+Right move right

bindsym $M-C-S+Left  move workspace to output left
bindsym $M-C-S+Down  move workspace to output down
bindsym $M-C-S+Up    move workspace to output up
bindsym $M-C-S+Right move workspace to output right

# split focus
  #not sure how to use this...
bindsym $M+b split h
bindsym $M+v split v
#+end_src
** wofi
A launcher/menu program for wlroots based wayland compositors such as sway. Similar functionality to rofi

| Keys     | description     | extra |
|----------+-----------------+-------|
| $M+Tab   | Launches Wofi   | drun  |
| $M-S+Tab | Window switcher |       |
| $M-C+Tab | Emoji selector  |       |
| $M-A+Tab | Wifi menu       |       |

#+begin_src conf
#----=[ wofi ]=----#
set $menu wofi | xargs swaymsg exec --
    
bindsym $M+Tab exec $menu 
bindsym $M-S+Tab exec "python ~/.config/wofi/windows.py"
bindsym $M-C+Tab exec "wofi-emoji"
# bindsym $M-C-A+Tab exec "python ~/.config/wofi/ssh.py 'alacritty -t'"
bindsym $M-A+Tab exec "bash ~/.config/wofi/wofi-wifi-menu.sh"
#+end_src

** Function & Media keys
#+begin_src conf
#----=[ Media keys ]=----#
bindsym XF86AudioMute         exec "pamixer -t"
bindsym XF86AudioLowerVolume  exec "pamixer -d 5 --allow-boost"
bindsym XF86AudioRaiseVolume  exec "pamixer -i 5 --allow-boost"
bindsym XF86MonBrightnessDown exec "brightnessctl set -10%"
bindsym XF86MonBrightnessUp   exec "brightnessctl set +10%"

bindsym $M+XF86poweroff   exec $lock
bindsym $M-S+XF86poweroff exec systemctl suspend

#----=[ Function Keys ]=----#	
bindsym $M+f11 fullscreen
#+end_src
** Workspaces
~workspace_auto_back_and_forth~ enables using the same key binding to switch back to previous workspace.
- Assume you are using workspace 1 and use M+2 to move to workspace 2. Pressing M+2 again moves back to workspace 1.
#+begin_src conf
### Workspaces
workspace_auto_back_and_forth yes 

bindsym $M+1 workspace number 1
bindsym $M+2 workspace number 2
bindsym $M+3 workspace number 3
bindsym $M+4 workspace number 4
bindsym $M+5 workspace number 5
bindsym $M+6 workspace number 6
bindsym $M+7 workspace number 7
bindsym $M+8 workspace number 8
bindsym $M+9 workspace number 9
bindsym $M+0 workspace number 10

# Move focused container to workspace
bindsym $M-S+1 move container to workspace number 1
bindsym $M-S+2 move container to workspace number 2
bindsym $M-S+3 move container to workspace number 3
bindsym $M-S+4 move container to workspace number 4
bindsym $M-S+5 move container to workspace number 5
bindsym $M-S+6 move container to workspace number 6
bindsym $M-S+7 move container to workspace number 7
bindsym $M-S+8 move container to workspace number 8
bindsym $M-S+9 move container to workspace number 9
bindsym $M-S+0 move container to workspace number 10

# bindsym $M-C+1 move workspace to output $output_screen2 
# bindsym $M+p move workspace to output $output_screen2; move workspace number 2 to output $output_screen1
# bindsym $M-S+p move workspace to output $output_screen1; move workspace number 2 to output $output_screen2
# bindsym $M+p exec /home/MoonDough/.config/sway/scripts/SwapWorkspaces.sh
#+end_src
*** Swapping workspaces
#+begin_src sh :tangle scripts/SwapWorkspaces.sh :mkdirp yes :tangle-mode (identity #o755)
#!/bin/fish
output_1_name=$(swaymsg -t get_outputs --raw | jq '.[0].name' -r)
output_2_name=$(swaymsg -t get_outputs --raw | jq '.[1].name' -r)
workspace_on_1=$(swaymsg -t get_outputs --raw | jq '.[0].current_workspace' -r)
workspace_on_2=$(swaymsg -t get_outputs --raw | jq '.[1].current_workspace' -r)
swaymsg workspace $workspace_on_1
swaymsg move workspace to output $output_2_name
swaymsg workspace $workspace_on_2
swaymsg move workspace to output $output_1_name
#+end_src
** Application Shortcuts
#+begin_src conf
#----=[ App Shortcuts ]=----#
bindsym $M-S+space exec $term
bindsym $M-A+Return exec $browser1
bindsym $M-S+Return exec $browser2
#+end_src
* Inputs & outputs
** Input
input names by running: ~swaymsg -t get_inputs~
#+begin_src conf
### Input configuration
#----=[ keyboard ]=---#
input "type:keyboard" {
      xkb_layout us
      xkb_options ctrl:nocaps
}

#----=[ Touch Screen ]=----#
set $input_screen1 1267:11290:ELAN9008:00_04F3:2C1A
set $input_screen2 1267:11291:ELAN9009:00_04F3:2C1B

input $input_screen1 {
      map_to_output  eDP-1      
      dwt enabled
      tap enabled
      middle_emulation enabled
      natural_scroll enabled
      scroll_method two_finger
}
input $input_screen2 {
      map_to_output  DP-3      
      dwt enabled
      tap enabled
      middle_emulation enabled
      natural_scroll enabled
      scroll_method two_finger
}
#+end_src
** Output
#+begin_center
output names by running: ~swaymsg -t get_outputs~
#+end_center

Most of the configuration was moved to Kanshi

#+begin_src conf
### Outputs
# output * bg $(find $wallpapers_path -type f | shuf -n 1) fill
#----=[ Monitors ]=----#
set $output_screen1 eDP-1
set $output_screen2 DP-3
set $output_hdmi HDMI-A-1
    
#--- Setting wallpaper
output $output_hdmi    bg $(find $wallpapers_path -type f | shuf -n 1) fill
output $output_screen1 bg $(find $wallpapers_path -type f | shuf -n 1) fill 
output $output_screen2 bg $(find $wallpapers_path -type f | shuf -n 1) fill
focus output $output_screen1
#+end_src
*** Kanshi
#+begin_src conf :tangle ~/.config/kanshi/config
profile {
	output eDP-1 mode 1920x1080 position 0,0
	output DP-3  mode 1920x515 position 0,1080
}
profile {
	output "Goldstar Company Ltd LG Ultra HD 0x00006A1A" mode 3840x2160 position 0,0 
	output eDP-1 mode 1920x1080 position 3840,0 
	output DP-3 mode 1920x515 position 3840,1080 
}
#+end_src
* Idle
This will lock your screen after 300 seconds of inactivity, then turn off your displays after another 300 seconds, and turn your screens back on when resumed. It will also lock your screen before your computer goes to sleep.
#+begin_src conf
exec swayidle -w \
     timeout 300 'swaylock-fancy -t $USER' \
     timeout 600 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
     before-sleep 'swaylock-fancy -t $USER'
#+end_src
* Modes
** Layout
#+begin_src conf
#----=[ Mode: Layout ]=----#
set $Layout "Layout: |(d) stacking|(t) tabbed|(s) split|(v) Vsplit|(h) Hsplit"
mode $Layout {
     bindsym --to-code {
       d layout stacking     ; mode "default"
       t layout tabbed       ; mode "default"
       s layout toggle split ; mode "default"
       v layout splitv       ; mode "default"
       h layout splith       ; mode "default"

       # Exit mode
       Return mode "default"
       Escape mode "default"
     }
}
bindsym $M+F1 mode $Layout
#+end_src
** Resize
#+begin_src conf
#----=[ Mode: Resize]=----#
set $Resize "Resize: ()|()|()|()"
mode $Resize {
     bindsym --to-code {
     # using [left|up|down|right]
       Left  resize shrink width  10px
       Down  resize grow   height 10px
       Up    resize shrink height 10px
       Right resize grow   width  10px

       # Exit mode
       Return mode "default"
       Escape mode "default"
     }
}
bindsym $M+r mode $Resize
#+end_src
** Power
#+begin_src conf
#----=[ Mode: Power]=----#
set $Power "Power: (Power) Shutdown|(F1) Reboot|(F2) Lock"
mode $Power {
     bindsym --to-code {
       XF86poweroff exec "systemctl -i poweroff"
       F1 exec "systemctl -i reboot"
       F2 exec $lock
       # Exit mode
       Return mode "default"
       Escape mode "default"
     }
}
bindsym XF86poweroff mode $Power
#+end_src
* Scratch pad
Sway has a "scratchpad", which is a "bag of holding" for windows. You can send windows there and get them back later.

| Key | description | Extra |
|-----+-------------+-------|
|     |             |       |

#+begin_src conf
#----=[ scratchpads ]=----#
set $scratch_pos {
    border none
    move position center
    resize set width 97 ppt height 95 ppt
}
bindsym $M-S+minus move scratchpad
bindsym $M+minus scratchpad show
#+end_src
** Always Scratch pad
*** Music
#+begin_src conf
#---- Spotify
for_window [class="Spotify"] {
	   # floating enable
	   move scratchpad
 	   opacity set 0.8
	   sticky enable
	   scratchpad show
	   $scratch_pos
}
bindsym $M+m exec swaymsg [class="Spotify"] scratchpad show || exec "spotify", $scratch_pos
#+end_src
*** signal
#+begin_src conf
#---- Signal Messenger
for_window {
	   [class="Signal"] {
	   		    opacity set 0.75,
			    floating enable,
	   		    move scratchpad,
	   		    scratchpad show
			    $scratch_pos
	   }
}
# assign [class="Signal"] scratchpad
bindsym $M-A+m exec swaymsg [class="Signal"] scratchpad show || exec "signal-desktop", $scratch_pos
#+end_src
*** Firefox
#+begin_src conf
#---- Firefox
for_window [class="firefox"] {
	   inhibit_idle fullscreen
	   border none
	   floating enable
	   move scratchpad
	   sticky enable
	   scratchpad show
	   $scratch_pos
}
bindsym $M+Return exec swaymsg [class="firefox"] scratchpad show || exec firefox, $scratch_pos
#+end_src
*** discord
#+begin_src conf
#---- Discord
for_window [class="discord"] {
	   floating enable
	   move scratchpad
	   sticky enable
	   scratchpad show
	   $scratch_pos
}
bindsym $M+d exec swaymsg [class="discord"] scratchpad show || exec discord, $scratch_pos
#+end_src
*** calculator
#+begin_src conf
for_window [app_id="qalculate-gtk"] {
	   move scratchpad
	   sticky enable
	   scratchpad show
}
bindsym $M+n exec swaymsg [app_id="qalculate-gtk"] scratchpad show || exec qalculate-gtk
#+end_src
*** Termonad
#+begin_src conf
#---- Termonad
for_window [app_id="termonad"] {
	   opacity set 0.65
	   floating enable
	   move scratchpad
	   sticky enable
	   scratchpad show
	   $scratch_pos
}
bindsym $M+space exec swaymsg [app_id="termonad"] scratchpad show || exec termonad, $scratch_pos
#+end_src
*** Alacritty
#+begin_src conf
#---- Alacritty
for_window [app_id="Alacritty"] {
	   opacity set 0.7
	   floating enable
	   move scratchpad
	   sticky enable
	   scratchpad show
	   $scratch_pos
}
bindsym $M-A+space exec swaymsg [app_id="Alacritty"] scratchpad show || exec alacritty, $scratch_pos
#+end_src
*** Keepassxc
#+begin_src conf
#---- Keepassxc
for_window {
	   [app_id="org.keepassxc.KeePassXC"] {
	   				      move scratchpad,
	   				      scratchpad show,
					      $scratch_pos
	   }
} 
bindsym Control+Alt+Delete exec swaymsg [app_id="org.keepassxc.KeePassXC"] scratchpad show || exec "keepassxc", $scratch_pos
#+end_src
*** File Browser
#+begin_src conf
#---- Ranger
for_window {
		 [app_id="ranger"] {
		 		   opacity set 0.75,
		 		   move scratchpad,
				   scratchpad show
				   $scratch_pos
		 }
}
bindsym $M+f exec swaymsg [app_id="ranger"] scratchpad show || exec $term1 -t 'ranger' --class 'ranger' -e ranger, $scratch_pos
	
#+end_src
** Not Always Scratch pad
*** emacs
#+begin_src conf
bindsym $M+e exec swaymsg [class="Emacs"] scratchpad show || exec "$EDITOR", $scratch_pos
bindsym $M-S+e exec "$EDITOR"
#+end_src
* Window Rules
#+begin_src conf
for_window {
	   [window_role="pop-up"] {
	   			  floating enable
	   }
	   [window_role="dialog"] {
	   			  floating enable
	   }
	   [window_type="dialog"] {
	   			  floating enable
	   }
	   [class="Brave-browser"] {
	   			   border none
	   			   inhibit_idle fullscreen
	   }
	   [class="Emacs"] {
	   		   border none,
	   		   opacity set 0.75
	   }
	   [app_id="zoom"] {
	   		 inhibit_idle focus
			 floating enable
	   }
	   [app_id="nm-tray"] {
	   		      floating enable
	   }
}
#+end_src
* Bar
#+begin_src conf
bar {
    # output $output_screen1
    swaybar_command waybar
}

include @sysconfdir@/sway/config.d/*
#+end_src
